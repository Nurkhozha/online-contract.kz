<footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12 fbox">
                    <h4>Online-contract.kz</h4>
                    <p class="text">Полный цикл от заключения договоров ... онлайн</p>
                    <ul class="list-inline">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>                        
                    </ul>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12 fbox">
                    <h4>Как это работает?</h4>
                    <ul class="big">
                        <li><a href="business-processes.php" title="">Бизнес процессы</a></li>
                        <li><a href="digital-signature.php" title="">Закон об ЭЦП</a></li>
                        <li><a href="security.php" title="">Безопасность</a></li>
                        <li><a href="e-archive.php" title="">Электронный архив</a></li>
                    </ul>
                </div>
               
                <div class="col-md-3 col-sm-6 col-xs-12 fbox">
                    <h4>TOO "PBSOFT"</h4>
                    <p class="text">Более 10 лет опыта в области разработки корпоративных приложений. Мы делаем ваш бизнес профессиональней</p>
                    <p><a href="tel:+77172787032"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> 8 (7172) 787 032</a></p>
                    <p><a href="mailto:iletisim@agrisosgb.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> mail@awebsitename.com</a></p>
                </div>
            </div>
        </div>
        <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <p class="pull-left">&copy; 2018 PBSoft</p>
                    </div>
                    <div class="col-md-8">
                        <ul class="list-inline navbar-right">
                            <li><a href="index.php">ГЛАВНАЯ</a></li>
                            <li><a href="business-processes.php">КАК ЭТО РАБОТАЕТ?</a></li>
                            <li><a href="price.php">ЦЕНЫ</a></li>
                            <li><a href="faq.php">FAQ</a></li>
                            <li><a href="#">ПОПРОБОВАТЬ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>        
    </footer>