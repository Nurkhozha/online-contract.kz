<header class="site-header">      
        <nav class="navbar navbar-default navbar-fixed-top">
                <div class="top" style="background-color: #404040">
                        <div class="container">
                            <div class="row">                            
                                <div class="col-sm-6">
                                    <ul class="list-inline pull-left">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
                                        <li><a href="tel:+77172787032"><i class="fa fa-phone"></i> 8 (7172) 787 032</a></li>
                                    </ul>                        
                                </div>
                                <div class="col-sm-6">
                                    <ul class="list-inline pull-right">                                       
                                        <li><a href="#">Вход</a></li>
                                        <li><a href="#">Регистрация</a></li>                                               
                                    </ul>            
                                </div>
                            </div>
                        </div>
                    </div>
			<div class="container">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse">
					<span class="sr-only">Toggle Navigation</span>
					<i class="fa fa-bars"></i>
				</button>
				<a href="index.php" class="navbar-brand">
					<img src="img/logo.png" width="200" height="73" alt="Post">
				</a>               
                <div class="collapse navbar-collapse" id="bs-navbar-collapse">
                    <ul class="nav navbar-nav main-navbar-nav">
                        <li><a href="index.php" title="">ГЛАВНАЯ</a></li>                        
                        <li><a href="business-processes.php" title="">КАК ЭТО РАБОТАЕТ?</a></li>
                        <li><a href="price.php" title="">ЦЕНЫ</a></li>
                        <li><a href="faq.php" title="">FAQ</a></li>
                        <li><a href="#" title="">ПОПРОБОВАТЬ</a></li>
                    </ul>                           
                </div>
			</div>
		</nav>        
    </header>