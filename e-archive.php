<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Online-Contract.kz - Бизнес процессы</title>
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&amp;subset=latin-ext" rel="stylesheet">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php
    include "header.php";
?>       
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li class="active">Как это работает? Электронный архив</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>
    <main class="site-main page-main">        

        <div class="container">
            <div class="row">     
            <aside class="sidebar col-sm-3">
                <div class="widget">
                    <h4>Как это работает?</h4>
                    <ul>
                        <li><a href="business-processes.php" title="">Бизнес процессы</a></li>
                        <li><a href="digital-signature.php" title="">Закон об ЭЦП</a></li>
                        <li><a href="security.php" title="">Безопасность</a></li>
                        <li class="current"><a href="e-archive.php" title="">Электронный архив</a></li>
                    </ul>
                </div>
            </aside>      
            <section class="page right-div col-sm-9"> <h2 class="sub-title">Электронный архив</h2>
                <div class="col-sm-12">                   
                    <div class="entry">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipiscing elit. Etiam congue lectus diam, sit amet cursus massa efficitur sed. Pellentesque efficitur dolor tellus, sit amet vestibulum leo facilisis ac. Nullam vitae fringilla neque. Aliquam erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                </div>                                     
                <div class="col-sm-12">                   
                    <div class="entry">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipiscing elit. Etiam congue lectus diam, sit amet cursus massa efficitur sed. Pellentesque efficitur dolor tellus, sit amet vestibulum leo facilisis ac. Nullam vitae fringilla neque. Aliquam erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                </div>                                     
                <div class="col-sm-12">                   
                    <div class="entry">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipiscing elit. Etiam congue lectus diam, sit amet cursus massa efficitur sed. Pellentesque efficitur dolor tellus, sit amet vestibulum leo facilisis ac. Nullam vitae fringilla neque. Aliquam erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                </div>                                     
            </section>      
        </div>            
    </div>
</main>
<?php
    include "footer.php";
?>
    