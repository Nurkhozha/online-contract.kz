<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Company NAME</title>
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&amp;subset=latin-ext" rel="stylesheet">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <?php
        include "header.php";
    ?>
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#" title="Post">Главная</a></li>
                        <li class="active">Цены</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>    
    <main class="site-main page-main">
        <section class="pricing-table  section" id="pricing">
		<div class="container">
			<!--<div class="row">
				<div class="col">
					<div class="title text-center">
						<h4>Easy Pricing</h4>
						<h2>Pricing.</h2>
						<span class="border"></span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum reiciendis quasi itaque, obcaecati atque sit!</p>
					</div>
				</div>
			</div>-->
			<div class="row">
				<!-- single pricing table -->
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="pricing-item">
						
						<!-- plan name & value -->
						<div class="price-title bg-pricing">
							<h3>Basic</h3>
							<strong class="value">$99</strong>
							<p>Perfect for single freelancers who work by themselves</p>
						</div>
						<!-- /plan name & value -->
						
						<!-- plan description -->
						<ul>
							<li>1GB Disk Space</li>
							<li>10 Email Account</li>
							<li>Script Installer</li>
							<li>1 GB Storage</li>
							<li>10 GB Bandwidth</li>
							<li>24/7 Tech Support</li>
						</ul>
						<!-- /plan description -->
						
						<!-- signup button -->
						<!--<a class="btn btn-main" href="#">Войти</a>-->
						<!-- /signup button -->
						
					</div>
				</div>
				<!-- end single pricing table -->
				
				<!-- single pricing table -->
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="pricing-item">
					
						<!-- plan name & value -->
						<div class="price-title  bg-pricing">
							<h3>Advance</h3>
							<strong class="value">$199</strong>
							<p>Suitable for small businesses with up to 5 employees</p>
						</div>
						<!-- /plan name & value -->
						
						<!-- plan description -->
						<ul>
							<li>1GB Disk Space</li>
							<li>10 Email Account</li>
							<li>Script Installer</li>
							<li>1 GB Storage</li>
							<li>10 GB Bandwidth</li>
							<li>24/7 Tech Support</li>
						</ul>
						<!-- /plan description -->
						
						<!-- signup button -->
						<!--<a class="btn btn-main" href="#">Войти</a>-->	
						<!-- /signup button -->
						
					</div>
				</div>
				<!-- end single pricing table -->
				
				<!-- single pricing table -->
				<div class="col-md-4 col-sm-6 col-xs-12 mx-auto">
					<div class="pricing-item ">
						
						<!-- plan name & value -->
						<div class="price-title bg-pricing">
							<h3>Professional</h3>
							<strong class="value">$299</strong>
							<p>Great for large businesses with more than 5 employees</p>
						</div>
						<!-- /plan name & value -->
						
						<!-- plan description -->
						<ul>
							<li>1GB Disk Space</li>
							<li>10 Email Account</li>
							<li>Script Installer</li>
							<li>1 GB Storage</li>
							<li>10 GB Bandwidth</li>
							<li>24/7 Tech Support</li>
						</ul>
						<!-- /plan description -->
						
						<!-- signup button -->
						<!--<a class="btn btn-main" href="#">Войти</a>-->
						<!-- /signup button -->
						
					</div>
				</div>
				<!-- end single pricing table -->
				
				
			</div>       <!-- End row -->
		</div>   	<!-- End container -->
	</section>
</main>
<?php
    include "footer.php";
?>