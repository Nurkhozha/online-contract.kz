<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Online-Contract.kz - Бизнес процессы</title>
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&amp;subset=latin-ext" rel="stylesheet">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<?php
    include "header.php";
?>       
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Главная</a></li>
                        <li class="active">Как это работает? Бизнес процессы</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>
    <main class="site-main page-main">        

        <div class="container">
            <div class="row">     
            <aside class="sidebar col-sm-3">
                <div class="widget">
                    <h4>Как это работает?</h4>
                    <ul>
                        <li class="current"><a href="business-processes.php" title="">Бизнес процессы</a></li>
                        <li><a href="digital-signature.php" title="">Закон об ЭЦП</a></li>
                        <li><a href="security.php" title="">Безопасность</a></li>
                        <li><a href="e-archive.php" title="">Электронный архив</a></li>
                    </ul>
                </div>
            </aside>      
            <section class="page right-div col-sm-9"> <h2 class="sub-title">Бизнес процессы</h2>
                <div class="col-sm-6"><h2 class="sub_title">1</h2> <!--1 бп-->
                    <ul class="list-ic vertical">
                        <li id="contractLink">
                            <span>1</span>
                            <a>Договор</a>
                        </li>
                        <li id="schetNaOplatuLink">
                            <span>2</span>
                            <a>Счет на оплату</a>
                        </li>
                        <li id="schetFacturaLink">
                            <span>3</span>
                            <a>Счет фактура</a>
                        </li>
                        <li id="aktVRLink">
                            <span>4</span>
                            <a>Акт выполненных работ</a>
                        </li>
                    </ul>                   
                </div>
                <div class="col-sm-6"> <!--Описание 1 бп-->
                    <h2 id="firstTitle" class="page-title">Договор на окозание услуг (работ)</h2>
                    <div id="defaultText" class="entry">
                        <p>Default erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                    <div id="contractText" class="entry"style="display:none;" >
                        <p>Договор erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                    <div id="schetNaOplatuText"  style="display:none;" class="entry">
                        <p>Счет на оплату, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipiscing elit. Etiam congue lectus diam, sit amet cursus massa efficitur sed. Pellentesque efficitur dolor tellus, sit amet vestibulum leo facilisis ac. Nullam vitae fringilla neque. Aliquam erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                    <div id="schetFacturaText"  style="display:none;" class="entry">
                        <p>Счет фактура, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipiscing elit. Etiam congue lectus diam, sit amet cursus massa efficitur sed. Pellentesque efficitur dolor tellus, sit amet vestibulum leo facilisis ac. Nullam vitae fringilla neque. Aliquam erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                    <div id="aktVRText"  style="display:none;" class="entry">
                        <p>aktVRText фактура, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipiscing elit. Etiam congue lectus diam, sit amet cursus massa efficitur sed. Pellentesque efficitur dolor tellus, sit amet vestibulum leo facilisis ac. Nullam vitae fringilla neque. Aliquam erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                </div>                                     
            </section>                        

            <section class="page right-div col-sm-9">
                <div class="col-sm-6"><h2 class="sub_title">2</h2> <!--1 бп-->
                    <ul class="list-ic vertical">
                        <li id="contractLink1">
                            <span>1</span>
                            <a>Договор</a>
                        </li>
                        <li id="schetNaOplatuLink1">
                            <span>2</span>
                            <a>Счет на оплату</a>
                        </li>
                        <li id="doverennostLink">
                            <span>3</span>
                            <a>Доверенность</a>
                        </li>
                        <li id="nakladnayaLink">
                            <span>4</span>
                            <a>Накладная</a>
                        </li>
                    </ul>                   
                </div>
                <div class="col-sm-6"> <!--Описание 1 бп-->
                    <h2 id="secondTitle" class="page-title">CONTENT TITLE</h2>
                    <div id="defaultText" class="entry">
                        <p>Default erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                    <div id="contractText1" class="entry"style="display:none;" >
                        <p>Договор erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                    <div id="schetNaOplatuText1"  style="display:none;" class="entry">
                        <p>Счет на оплату, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipiscing elit. Etiam congue lectus diam, sit amet cursus massa efficitur sed. Pellentesque efficitur dolor tellus, sit amet vestibulum leo facilisis ac. Nullam vitae fringilla neque. Aliquam erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                    <div id="doverennostText"  style="display:none;" class="entry">
                        <p>Доверенность, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipiscing elit. Etiam congue lectus diam, sit amet cursus massa efficitur sed. Pellentesque efficitur dolor tellus, sit amet vestibulum leo facilisis ac. Nullam vitae fringilla neque. Aliquam erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                    <div id="nakladnayaText"  style="display:none;" class="entry">
                        <p>Накладная фактура, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipisdolor sit amet, consectetur adipiscing elit. Etiam congue lectus diam, sit amet cursus massa efficitur sed. Pellentesque efficitur dolor tellus, sit amet vestibulum leo facilisis ac. Nullam vitae fringilla neque. Aliquam erat nunc, vestibulum at suscipit quis, consequat nec lorem. Phasellus porttitor mollis fermentum. Nulla eu eros libero. Donec semper, urna a rhoncus rutrum, nulla neque dignissim neque, nec congue tortor enim id mauris. Nulla euismod dolor lacus, porttitor imperdiet elit ornare nec. In finibus non justo id viverra.</p>
                    </div>
                </div>                                    
            </section>    
        </div>            
    </div>
</main>
<?php
    include "footer.php";
?>
<script>
    $('#contractLink').on('click', function () {
        if($('#contractText').css('display')=='none') {
            $('#contractText').show().siblings('div').hide();
            $('#contractLink').addClass('activebp').siblings('li').removeClass('activebp');     
        }else{
            $('#contractText').siblings('div').first().show().siblings('div').hide();
            $('#contractLink').removeClass('activebp');
        }
    })

    $('#schetNaOplatuLink').on('click', function () {
        if($('#schetNaOplatuText').css('display')=='none') {
            $('#schetNaOplatuText').show().siblings('div').hide();
            $('#schetNaOplatuLink').addClass('activebp').siblings('li').removeClass('activebp');    
        }else{
            $('#schetNaOplatuText').siblings('div').first().show().siblings('div').hide();
            $('#schetNaOplatuLink').removeClass('activebp');
        }
    })

     $('#schetFacturaLink').on('click', function () {
        if($('#schetFacturaText').css('display')=='none') {
            $('#schetFacturaText').show().siblings('div').hide();
            $('#schetFacturaLink').addClass('activebp').siblings('li').removeClass('activebp');    
        }else{
            $('#schetFacturaText').siblings('div').first().show().siblings('div').hide();
            $('#schetFacturaLink').removeClass('activebp');
        }
    })

     $('#aktVRLink').on('click', function () {
        if($('#aktVRText').css('display')=='none') {
            $('#aktVRText').show().siblings('div').hide();
            $('#aktVRLink').addClass('activebp').siblings('li').removeClass('activebp');    
        }else{
            $('#aktVRText').siblings('div').first().show().siblings('div').hide();
            $('#aktVRLink').removeClass('activebp');
        }
    })


    $('#contractLink1').on('click', function () {
        if($('#contractText1').css('display')=='none') {
            $('#contractText1').show().siblings('div').hide();
            $('#contractLink1').addClass('activebp').siblings('li').removeClass('activebp');     
        }else{
            $('#contractText1').siblings('div').first().show().siblings('div').hide();
            $('#contractLink1').removeClass('activebp');
        }
    })

    $('#schetNaOplatuLink1').on('click', function () {
        if($('#schetNaOplatuText1').css('display')=='none') {
            $('#schetNaOplatuText1').show().siblings('div').hide();
            $('#schetNaOplatuLink1').addClass('activebp').siblings('li').removeClass('activebp');    
        }else{
            $('#schetNaOplatuText1').siblings('div').first().show().siblings('div').hide();
            $('#schetNaOplatuLink1').removeClass('activebp');
        }
    })

     $('#doverennostLink').on('click', function () {
        if($('#doverennostText').css('display')=='none') {
            $('#doverennostText').show().siblings('div').hide();
            $('#doverennostLink').addClass('activebp').siblings('li').removeClass('activebp');    
        }else{
            $('#doverennostText').siblings('div').first().show().siblings('div').hide();
            $('#doverennostLink').removeClass('activebp');
        }
    })

     $('#nakladnayaLink').on('click', function () {
        if($('#nakladnayaText').css('display')=='none') {
            $('#nakladnayaText').show().siblings('div').hide();
            $('#nakladnayaLink').addClass('activebp').siblings('li').removeClass('activebp');    
        }else{
            $('#nakladnayaText').siblings('div').first().show().siblings('div').hide();
            $('#nakladnayaLink').removeClass('activebp');
        }
    })
</script>