<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Online-Contract.kz - Онлайн-контрактование</title>
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&amp;subset=latin-ext" rel="stylesheet">
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>    
    <?php 
        include "header.php"; 
    ?>
    <main class="site-main" style="margin-top: 93px;">
        <section class="hero_area">
            <div class="hero_content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <h1>Онлайн-контрактование</h1>
                            <h2>Полный цикл от заключения договоров ... онлайн</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="services">
            <h2 class="section-title">ДОКУМЕНТЫ</h2>
            <p class="desc">Praesent faucibus ipsum at sodales blandit</p>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-file-o"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">ДОГОВОРА</h4>
                                <p>Cоглашение двух или более лиц об установлении, изменении или прекращении гражданских прав и обязанностей.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-file-code-o"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">СЧЕТ - ФАКТУРЫ</h4>
                                <p>Документ, удостоверяющий фактическую отгрузку товаров или оказание услуг и их стоимость.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-percent"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">СЧЕТ - НА ОПЛАТУ</h4>
                                <p>Необязательный документ, содержащий платежные реквизиты, по которым плательщик осуществляет перевод денежных средств.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-check-square-o"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">АКТЫ ВЫПОЛНЕННЫХ РАБОТ</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id pulvinar magna. Aenean accumsan iaculis lorem, nec sodales lectus auctor tempor.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-file-text-o"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">ДОВЕРЕННОСТЬ</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id pulvinar magna. Aenean accumsan iaculis lorem, nec sodales lectus auctor tempor.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-archive"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">НАКЛАДНАЯ</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id pulvinar magna. Aenean accumsan iaculis lorem, nec sodales lectus auctor tempor.</p>
                            </div>
                        </div>
                    </div>                     
                </div>
            </div>
        </section>
        <section class="home-area">
            <div class="home_content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12"><h2 class="sub_title">ПРЕИМУЩЕСТВА</h2></div>
                        <div class="home_list">
                            <ul>
                                <li class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="thumbnail">
                                        <img src="img/h1.png" width="150px" alt="Post">
                                        <div class="caption">
                                            <h3><center>БЫСТРО</center></h3>
                                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                                        </div>
                                    </div>                                        
                                </li>
                                <li class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="thumbnail">
                                        <img src="img/h2.png" width="150px" class="img-responsive" alt="Post">
                                        <div class="caption">
                                            <h3><center>БЕЗОПАСНО</center></h3>
                                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                                        </div>
                                    </div>                                        
                                </li>
                                <li class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="thumbnail">
                                        <img src="img/h3.png" width="150px" class="img-responsive" alt="Post">
                                        <div class="caption">
                                            <h3><center>ЭКОНОМИЧНО</center></h3>
                                            <p>Сокращаются расходы на печать, доставку и хранение документов</p>
                                        </div>
                                    </div>                                        
                                </li>
                                <li class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="thumbnail">
                                        <img src="img/h4.png" width="150px" class="img-responsive" alt="Post">
                                        <div class="caption">
                                            <h3><center>ОНЛАЙН</center></h3>
                                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus.</p>
                                        </div>
                                    </div>                                        
                                </li>                                    
                            </ul>
                        </div>
                        <div class="col-sm-12">
                            
                            <h2 class="sub_title w10">ЕСТЬ ВОПРОСЫ? -СВЯЖИТЕСЬ С НАМИ</h2>
                            <div class="clearfix"></div>
                            <div class="container">

                                <div class="row">
                                    <!-- Contact Details -->
                                    <div class="col-12 col-md-6">
                                    <a class="dg-widget-link" href="http://2gis.kz/astana/firm/70000001018089654/center/71.44246101379396,51.164688618193715/zoom/18?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Астаны</a>
                                    <div class="dg-widget-link">
                                        <a href="http://2gis.kz/astana/firm/70000001018089654/photos/70000001018089654/center/71.44246101379396,51.164688618193715/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии компании</a>
                                    </div>
                                    <div class="dg-widget-link">
                                        <a href="http://2gis.kz/astana/center/71.442438,51.164607/zoom/18/routeTab/rsType/bus/to/71.442438,51.164607╎Алма-Ата, деловой дом?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Алма-Ата, деловой дом</a>
                                    </div>
                                    <script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
                                    <script charset="utf-8">
                                        new DGWidgetLoader({"width":'100%',"height":400,"borderColor":"#a3a3a3","pos":{"lat":51.164688618193715,"lon":71.44246101379396,"zoom":16},"opt":{"city":"astana"},"org":[{"id":"70000001018089654"}]});
                                    </script>
                                    </div>
                                    <!-- / End Contact Details -->
                                        
                                    <!-- Contact Form -->
                                    <div class="contact-form col-12 col-md-6 ">
                                        <form id="contact-form" method="post" action="sendmail.php" role="form">
                                        
                                            <div class="form-group">
                                                <input type="text" placeholder="Имя" class="form-control" name="name" id="name">
                                            </div>
                                            
                                            <div class="form-group">
                                                <input type="email" placeholder="Email" class="form-control" name="email" id="email">
                                            </div>
                                            
                                            <div class="form-group">
                                                <input type="text" placeholder="Тема" class="form-control" name="subject" id="subject">
                                            </div>
                                            
                                            <div class="form-group">
                                                <textarea rows="6" placeholder="Сообщение" class="form-control" name="message" id="message"></textarea>	
                                            </div>
                                            
                                            <div id="mail-success" class="success">
                                                Thank you. The Mailman is on His Way :)
                                            </div>
                                            
                                            <div id="mail-fail" class="error">
                                                Sorry, don't know what happened. Try later :(
                                            </div>
                                            
                                            <div id="cf-submit">
                                                <input type="submit" id="contact-submit" class="btn btn-transparent" value="Отправить">
                                            </div>						
                                            
                                        </form>
                                    </div>
                                    <!-- ./End Contact Form -->
                                </div> <!-- end row -->                                                           
                            </div>                           
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </main>
    <?php
        include "footer.php";
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $('.carousel[data-type="multi"] .item').each(function(){
          var next = $(this).next();
          if (!next.length) {
            next = $(this).siblings(':first');
          }
          next.children(':first-child').clone().appendTo($(this));

          for (var i=0;i<4;i++) {
            next=next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
          }
        });        
    </script>   
</body>
</html>